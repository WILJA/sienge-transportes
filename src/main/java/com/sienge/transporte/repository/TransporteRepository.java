package com.sienge.transporte.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.sienge.transporte.domain.Transporte;

@RepositoryRestResource(exported = false)
public interface TransporteRepository extends JpaRepository<Transporte, Long> {	

	Page<Transporte> findByDescricaoContaining(String descricao, Pageable pageable);
	
	
}
