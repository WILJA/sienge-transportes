package com.sienge.transporte;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.sienge.transporte.domain.Rodovia;
import com.sienge.transporte.domain.TipoRodovia;
import com.sienge.transporte.domain.TipoVeiculo;
import com.sienge.transporte.domain.Transporte;
import com.sienge.transporte.domain.Veiculo;
import com.sienge.transporte.repository.RodoviaRepository;
import com.sienge.transporte.repository.TipoRodoviaRepository;
import com.sienge.transporte.repository.TipoVeiculoRepository;
import com.sienge.transporte.repository.TransporteRepository;
import com.sienge.transporte.repository.VeiculoRepository;

@Component
public class DataLoader implements ApplicationRunner {

	@Autowired
	TipoVeiculoRepository tipoVeiculoRepository;
	
	@Autowired
	TipoRodoviaRepository tipoRodoviaRepository;
	
	@Autowired
	RodoviaRepository rodoviaRepository;
	
	@Autowired
	VeiculoRepository veiculoRepository;
	
	@Autowired
	TransporteRepository transporteRepository;

	@Override
	public void run(ApplicationArguments arg0) throws Exception {
		tipoVeiculoRepository.save(new TipoVeiculo(1L, "Caminhão  baú", 1.00 ));
		tipoVeiculoRepository.save(new TipoVeiculo(2L, "Caminhão  caçamba", 1.05 ));
		tipoVeiculoRepository.save(new TipoVeiculo(3L, "Carreta", 1.12 ));
		
		tipoRodoviaRepository.save(new TipoRodovia(1L, "Pavimentada", 0.54 ));
		tipoRodoviaRepository.save(new TipoRodovia(2L, "Não-pavimentada", 0.62 ));
		
		rodoviaRepository.save(new Rodovia(1L, "BR-101", tipoRodoviaRepository.findOne(1L)));
		rodoviaRepository.save(new Rodovia(2L, "BR-282", tipoRodoviaRepository.findOne(1L)));
		rodoviaRepository.save(new Rodovia(3L, "SC-470", tipoRodoviaRepository.findOne(1L)));
		rodoviaRepository.save(new Rodovia(4L, "BR-319", tipoRodoviaRepository.findOne(1L)));
		rodoviaRepository.save(new Rodovia(5L, "SP-510", tipoRodoviaRepository.findOne(1L)));
		rodoviaRepository.save(new Rodovia(5L, "SP-1133", tipoRodoviaRepository.findOne(2L)));
		rodoviaRepository.save(new Rodovia(6L, "SP-5434", tipoRodoviaRepository.findOne(2L)));
		rodoviaRepository.save(new Rodovia(7L, "RS-3232", tipoRodoviaRepository.findOne(2L)));
		rodoviaRepository.save(new Rodovia(8L, "PR-5230", tipoRodoviaRepository.findOne(2L)));
		rodoviaRepository.save(new Rodovia(9L, "AC-5101", tipoRodoviaRepository.findOne(2L)));
		rodoviaRepository.save(new Rodovia(10L, "GO-7680", tipoRodoviaRepository.findOne(2L)));
		rodoviaRepository.save(new Rodovia(11L, "DF-1577", tipoRodoviaRepository.findOne(2L)));
		rodoviaRepository.save(new Rodovia(12L, "MT-9988", tipoRodoviaRepository.findOne(2L)));
		rodoviaRepository.save(new Rodovia(13L, "TO-4542", tipoRodoviaRepository.findOne(2L)));
		
		
		veiculoRepository.save(new Veiculo(1L, "DAILY CHASSI 35.10/ 35.13/ 40.13", "IVECO","QQQ 1234", "VERMELHO", "2010", "1213155DFDAAS", "TRUCK", tipoVeiculoRepository.findOne(1L)));
		veiculoRepository.save(new Veiculo(2L, "R-114 GA 380 6x2 NZ/4x2 3-Eixos", "SCANIA","QTQ 3523", "AZUL", "2012", "4343432FDF343", "TRUCK", tipoVeiculoRepository.findOne(1L)));
		veiculoRepository.save(new Veiculo(3L, "1114 3-Eixos", "MERCEDES-BENZ","QTH 1523", "BRANCO", "2002", "4343432FDF343", "TRUCK", tipoVeiculoRepository.findOne(1L)));
		veiculoRepository.save(new Veiculo(4L, "CARGO 2422/ 2422 E 3-Eixos", "FORD","QTG 3555", "VERDE", "2014", "4343432FDF343", "TRUCK", tipoVeiculoRepository.findOne(1L)));
		veiculoRepository.save(new Veiculo(5L, "FH-460 GLOBETROTTER 6x4 (E5)", "VOLVO","QYU 3532", "AMARELO", "2015", "4343432FDF343", "TRUCK", tipoVeiculoRepository.findOne(1L)));
		veiculoRepository.save(new Veiculo(6L, "190-T", "FIAT","QRR 3532", "PRETO", "2012", "4343432FDF343", "TRUCK", tipoVeiculoRepository.findOne(1L)));
		veiculoRepository.save(new Veiculo(7L, "AUMARK 6.50AK 3.8 4x2 TB(E5)", "FOTON","RTH 1235", "PRETO", "2013", "4343432FDF343", "TRUCK", tipoVeiculoRepository.findOne(1L)));
		veiculoRepository.save(new Veiculo(8L, "L-111", "SCANIA","JIJ 3678", "BRANCO", "2011", "4343432FDF343", "TRUCK", tipoVeiculoRepository.findOne(1L)));
		veiculoRepository.save(new Veiculo(9L, "R-114 GA 380 6x2 NZ/4x2 3-Eixos", "SCANIA","BRR 3523", "BRANCO", "2010", "4343432FDF343", "TRUCK", tipoVeiculoRepository.findOne(1L)));
		veiculoRepository.save(new Veiculo(10L, "R-440 A 6x4", "SCANIA","BYU 5432", "BRANCO", "2009", "4343432FDF343", "TRUCK", tipoVeiculoRepository.findOne(2L)));
		veiculoRepository.save(new Veiculo(11L, "AA1", "SAAB-SCANIA","NIN 6676", "BEGE", "2012", "4343432FDF343", "TRUCK", tipoVeiculoRepository.findOne(2L)));
		veiculoRepository.save(new Veiculo(12L, "BB2", "SAAB-SCANIA","JJI 5432", "BORDO", "2012", "4343432FDF343", "TRUCK", tipoVeiculoRepository.findOne(2L)));
		veiculoRepository.save(new Veiculo(13L, "CCC3", "SAAB-SCANIA","UIU 8767", "PURPURA", "2012", "4343432FDF343", "TRUCK", tipoVeiculoRepository.findOne(2L)));
		veiculoRepository.save(new Veiculo(14L, "R888", "SAAB-SCANIA","BVB 4356", "JUMBO", "2012", "4343432FDF343", "TRUCK", tipoVeiculoRepository.findOne(2L)));
		veiculoRepository.save(new Veiculo(15L, "V1000", "SAAB-SCANIA","HGG 4321", "METALICO", "2012", "4343432FDF343", "TRUCK", tipoVeiculoRepository.findOne(2L)));
		
		transporteRepository.save(new Transporte());
		
		

		
	}
}
