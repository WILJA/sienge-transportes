package com.sienge.transporte.dto;


import java.util.Set;



import com.sienge.transporte.domain.Rodovia;
import com.sienge.transporte.domain.Transporte;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode @ToString
public class TransporteRodoviaDto {

	@Getter @Setter
	Long id; 

	@Getter @Setter
    private Transporte transporte;

	@Getter @Setter
    private Set<Rodovia> rodovia;


    


}
