package com.sienge.transporte.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sienge.transporte.domain.Transporte;
import com.sienge.transporte.domain.TransporteRodovia;
import com.sienge.transporte.dto.TransporteRodoviaDto;
import com.sienge.transporte.exception.BusinessException;
import com.sienge.transporte.repository.TransporteRepository;
import com.sienge.transporte.repository.TransporteRodoviaRepository;



@Service
public class TransporteRodoviaService {
	
	@Autowired
	TransporteRepository transporteRepository;
	
	@Autowired
	TransporteRodoviaRepository transporteRodoviaRepository;
	
	@Transactional
	public List<TransporteRodovia> create(TransporteRodoviaDto trasnporteRodoviaDto)
	{
		
		if(trasnporteRodoviaDto == null){
			throw new BusinessException("Erro ao cadastrar informações.");
		}
		
		if(trasnporteRodoviaDto.getTransporte() == null){
			throw new BusinessException("Transporte não encotrado.");
		}
		
		if(trasnporteRodoviaDto.getTransporte().getDescricao() == null || trasnporteRodoviaDto.getTransporte().getDescricao().isEmpty()){
			throw new BusinessException("Descrição inválida.");
		}
		
		if(trasnporteRodoviaDto.getTransporte().getCarga() <= 0){
			throw new BusinessException("Carga inválida.");
		}
		
		if(trasnporteRodoviaDto.getTransporte().getVeiculo() == null){
			throw new BusinessException("Veículo inválido.");
		}	
		
		if(trasnporteRodoviaDto.getRodovia() == null || trasnporteRodoviaDto.getRodovia().isEmpty()){
			throw new BusinessException("Rodovia(s) não encontradas.");
		}
		
		
		trasnporteRodoviaDto.getTransporte().setDataGeracao(new Date());

		/// CALCULAR CUSTOS
		
		List<Double> custoRodovias = trasnporteRodoviaDto.getRodovia().stream().map(obj -> obj.getDistancia() * obj.getTipoRodovia().getCusto()).collect(Collectors.toList());
		List<Double> custoCargaRodovias = null;
		if(trasnporteRodoviaDto.getTransporte().getCarga() > 5){
			Integer cargaRodovia = trasnporteRodoviaDto.getTransporte().getCarga() - 5;
			Double cargaTaxaRodoviaPeso = cargaRodovia * 0.02;
			custoCargaRodovias = trasnporteRodoviaDto.getRodovia().stream().map(obj -> obj.getDistancia() * cargaTaxaRodoviaPeso).collect(Collectors.toList());
		}
		
		Double custo = custoRodovias.stream().collect(Collectors.summingDouble(Double::doubleValue));
		
		if(custoCargaRodovias != null && !custoCargaRodovias.isEmpty()) {
			Double custoPeso = custoCargaRodovias.stream().collect(Collectors.summingDouble(Double::doubleValue));
			custo += custoPeso;
		}
		
		if(custo == null){
			throw new BusinessException("Custo inválido.");
		}	
		
		trasnporteRodoviaDto.getTransporte().setCusto(custo);
		Transporte transpote = transporteRepository.saveAndFlush(trasnporteRodoviaDto.getTransporte());

		List<TransporteRodovia> transporteRodovias = new ArrayList<TransporteRodovia>();
		
		trasnporteRodoviaDto.getRodovia().forEach(obj ->{
			TransporteRodovia transporteRodovia = new TransporteRodovia();
		
			transporteRodovia.setTransporte(transpote);
			transporteRodovia.setRodovia(obj);
			transporteRodovia.setDistancia(obj.getDistancia());
			transporteRodovias.add(transporteRodovia);
		});
				
		return transporteRodoviaRepository.save(transporteRodovias);
	}
}
